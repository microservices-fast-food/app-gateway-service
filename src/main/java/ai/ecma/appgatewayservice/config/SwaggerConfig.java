package ai.ecma.appgatewayservice.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import springfox.documentation.swagger.web.SwaggerResource;
import springfox.documentation.swagger.web.SwaggerResourcesProvider;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableSwagger2
@Primary
public class SwaggerConfig implements SwaggerResourcesProvider {
    @Value("${spring.profiles.active}")
    private String profile;

    @Override
    public List<SwaggerResource> get() {
        List<SwaggerResource> resources = new ArrayList<>();
        if (profile.equals("prod")) {
            resources.add(swaggerResource("order-service", "http://localhost:8083/v2/api-docs", "2.0"));
            resources.add(swaggerResource("product-service", "http://localhost:8083/v2/api-docs", "2.0"));
            resources.add(swaggerResource("auth-service", "http://localhost:8083/v2/api-docs", "2.0"));
        }else {
            resources.add(swaggerResource("order-service", "http://localhost:82/v2/api-docs", "2.0"));
            resources.add(swaggerResource("product-service", "http://localhost:83/v2/api-docs", "2.0"));
            resources.add(swaggerResource("auth-service", "http://localhost:81/v2/api-docs", "2.0"));
//            resources.add(swaggerResource("auth-service", "https://4881-213-230-71-93.ngrok.io/v2/api-docs", "2.0"));
        }
        return resources;
    }

    private SwaggerResource swaggerResource(String name, String location, String version) {
        SwaggerResource swaggerResource = new SwaggerResource();
        swaggerResource.setName(name);
        swaggerResource.setLocation(location);
        swaggerResource.setSwaggerVersion(version);
        return swaggerResource;
    }
}
